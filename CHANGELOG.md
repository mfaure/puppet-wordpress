# Changelog

All notable changes to this project will be documented in this file.

## Release 1.0.3

**Features**

**Bugfixes**

  * #9 Add acceptence tests about ressources.
  * #10 Add acceptence tests about management of several instances.

**Known Issues**

  * #1 Fix attribute ensure set to latest from scratch with resources management.
  * #5 Permit to modify hour of update for external_fact.

## Release 1.0.2

**Features**

**Bugfixes**

  * #11 Remove lint setting `no-only_variable_string-check`.
  * #12 Write WP-CLI instead of wpcli in project description.

**Known Issues**

  * #1 Fix attribute ensure set to latest from scratch with resources management.
  * #5 Permit to modify hour of update for external_fact.

## Release 1.0.1

**Features**

**Bugfixes**

  * #7 Add missing keywords in metadata.json.
  * #8 Fix version in metadata.json.

**Known Issues**

  * #1 Fix attribute ensure set to latest from scratch with resources management.
  * #5 Permit to modify hour of update for external_fact.

## Release 1.0.0

**Features**

**Bugfixes**

  * #6 bump version to publish 1.0.0

**Known Issues**

  * #1 Fix attribute ensure set to latest from scratch with resources management.
  * #5 Permit to modify hour of update for external_fact.

## Release 0.2.0

**Features**

**Bugfixes**

  * #2 Fix license.
  * #3 Add informations in chapter `Limitations` in README.
  * #4 Update CHANGELOG.

**Known Issues**

  * #1 Fix attribute ensure set to latest from scratch with resources management.
  * #5 Permit to modify hour of update for external_fact.
